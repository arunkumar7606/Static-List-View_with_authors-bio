package com.example.aashu.listviewone;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class KumarVishwas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kumar_vishwas);

        Button kv1=findViewById(R.id.buttonkv);

        kv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i=new Intent(KumarVishwas.this,MainActivity.class);
                startActivity(i);
            }
        });
    }
}
