package com.example.aashu.listviewone;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listview1);
        final String data[] = getResources().getStringArray(R.array.jadu);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, data);

        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {


                //  String product = ((TextView) view).getText().toString();


                // Intent intent=new Intent(MainActivity.this,Munshi.class);
                //intent.putExtra("product" ,product);
                //startActivity(intent);

                Toast.makeText(MainActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                switch (position) {
                    case 0:
                        startActivity(new Intent(MainActivity.this, Munshi.class));
                        break;
                    case 1:
                        startActivity(new Intent(MainActivity.this, KabirDas.class));
                        break;
                    case 2:
                        startActivity(new Intent(MainActivity.this, TulsiDas.class));
                        break;
                    case 3:
                        startActivity(new Intent(MainActivity.this, RamdhariSingh.class));
                        break;
                    case 4:
                        startActivity(new Intent(MainActivity.this, JaishankarPrasad.class));
                        break;
                    case 5:
                        startActivity(new Intent(MainActivity.this, KumarVishwas.class));
                        break;
                    case 6:
                        startActivity(new Intent(MainActivity.this, ChetanBhagat.class));
                        break;
                    default:
                        startActivity(new Intent(MainActivity.this, KabirDas.class));


                }

//                Intent intent=new Intent(listView.getContext(),Munshi.class);
//                      listView.getContext().startActivity(intent);

            }
        });


    }
}
